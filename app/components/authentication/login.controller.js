(function() {
    'use strict';

    angular
        .module('windScripts')
        .controller('LoginController', LoginController);   
    
    LoginController.$inject = ['AuthService', 'store', '$state', '$rootScope'];
    
    function LoginController(AuthService, store, $state, $rootScope){
        var vm = this;        
        vm.message = 'Login';
        vm.login = login;
        vm.submitForm = submitForm;
        vm.waitForReturn = false;
        vm.returnDisplay = '';
        
        function submitForm (isValid, user) {            
            vm.waitForReturn = true;
            if (isValid)
                login(user);
        }
        
        function login (user){            
            AuthService.login(user)
                .then(function (data) {
                    console.log(data);
                    vm.waitForReturn = false;
                
                    if (!data.returnResult){
                        vm.returnDisplay = data.message;
                    } else {
                        $rootScope.$broadcast('isAuthenticated', {authenticated: true});                 
                        store.set('jwt', data.token);
                        if (!$state.params.ReturnUrl)
                            $state.go('home');
                        else
                            $state.go($state.params.ReturnUrl)
                    }
                    
                });
        }
        
    }
})();