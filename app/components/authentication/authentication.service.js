(function () {
    'use strict';

    angular
        .module('windScripts')
        .service('AuthService', AuthService);

    AuthService.$inject = ['$http', 'GLOBALS', 'store', '$state'];
    
    function AuthService($http, GLOBALS, store, $state) {
        return {
            login: login,
            signup: signup
        };
        
        
        function login (user) {
            return $http.post( GLOBALS().usersApi + 'login', user)
                .then(onSuccess)
                .catch(onError);
        }
        
        function signup (user) {
            return $http.post( GLOBALS().usersApi + 'signup', user)
                .then(onSuccess)
                .catch(onError);
        }
 
        function onSuccess (response) {
            return response.data;
        };
        
        function onError (error) {
            return error.data;
        };
    }
})();