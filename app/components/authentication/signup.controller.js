(function() {
    'use strict';

    angular
        .module('windScripts')
        .controller('SignupController', SignupController);   
    
    SignupController.$inject = ['AuthService', '$state', 'store', '$rootScope'];
    
    function SignupController(AuthService, $state, store, $rootScope){
        var vm = this;        
        vm.message = 'Sign up';
        vm.signup = signup;
        vm.submitForm = submitForm;
        vm.waitForReturn = false;
        vm.returnDisplay = '';
        
        function submitForm (isValid, user) {
            vm.waitForReturn = true;
            if (isValid)
                signup(user);
        }
        
        function signup (user){
            user.admin = true;
            AuthService.signup(user)
                .then(function (data) {
                    console.log(data);  
                    vm.waitForReturn = false;   
                    
                    if (!data.returnResult){
                        vm.returnDisplay = data.message;
                    } else {
                        $rootScope.$broadcast('isAuthenticated', {authenticated: true});  
                        store.set('jwt', data.token);
                        $state.go('home');  
                    }                    
                });
        }
        
    }
})();