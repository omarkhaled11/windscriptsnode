(function() {
    'use strict';

    angular
        .module('windScripts')
        .controller('TibiaScriptController', TibiaScriptController);

    TibiaScriptController.$inject = ['tibiaScriptService', 'MainService', 'CommonService', '$state', 'GLOBALS'];
    
    function TibiaScriptController(tibiaScriptService, MainService, CommonService, $state, GLOBALS){
        var vm = this;
        vm.script_Id = $state.params.script_Id;
        vm.getDownloadLink = getDownloadLink;
        vm.apiUrl = GLOBALS().downloadUrl;        
                            
        if (!MainService.allScripts){            
            getScriptData(vm.script_Id)
                .then (function (data){
                    vm.script = data;
                    fixVideoUrl(vm.script);
                });
            
            getAllData()
                .then (function (data) {          
                    MainService.allScripts = data;                    
                });
            
        } else {            
            vm.scriptsArray = MainService.allScripts;
            filterScript(vm.script_Id);            
        }                
                   
        
        
        function filterScript (script_Id) {            
            for (var i = 0; i < vm.scriptsArray.length; i++) {
                if (vm.scriptsArray[i]._id == script_Id) {
                    vm.script = vm.scriptsArray[i];
                    vm.script.Vocations = setVocationString(vm.script);                
                }                    
            }   
            console.log(vm.script);
        }
        
        
        function getDownloadLink (script_Id) {
            return vm.apiUrl + vm.script_Id;
        }
        
        
        function getScriptData (script_Id) {
            return tibiaScriptService.getScript(script_Id)                
                .then(function (data) {                    
                    return data;
                });
        }  
        
        function fixVideoUrl (script) {            
            script.VideoURL = script.VideoURL.replace("watch?v=", "v/");             
        }

                
        function getAllData () {
            return MainService.getScripts()
                .then(function (data) {
                    return data;
                });
        }   
        
        
        function setVocationString (script) {
            return CommonService.setVocationString(script);
        }    
           
                
    }
})();