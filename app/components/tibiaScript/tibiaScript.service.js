(function () {
    'use strict';

    angular
        .module('windScripts')
        .service('tibiaScriptService', tibiaScriptService);

    tibiaScriptService.$inject = ['$http', 'GLOBALS'];
    
    function tibiaScriptService ($http, GLOBALS) {
        return {
            getScript: getScript,
            downloadScript: downloadScript
        };

        function getScript(script_Id) {
            return $http.get(GLOBALS().apiUrl + script_Id)
                .then(onSuccess)
                .catch(onError);
        }
        
        function downloadScript (script_Id) {
            $http.get(GLOBALS().downloadUrl + script_Id)
//                .then(onSuccess)
//                .catch(onError);
        }
        
        function onSuccess (response) {
            return response.data;
        };
        
        function onError (error) {
            return error.data;
        };
    }
    
    
})();