(function () {
    'use strict';

    angular
        .module('windScripts')
        .service('CommonService', CommonService);

    CommonService.$inject = ['$http', 'GLOBALS'];
    
    function CommonService ($http, GLOBALS) {
        
        this.allScripts;
        
        return {
            setAllVocationStrings: setAllVocationStrings,
            setVocationString: setVocationString            
        };
        
        
        function setAllVocationStrings (allScripts) {
            for (var i = 0; i < allScripts.length; i++) {
                allScripts[i].Vocation = setVocationString(allScripts[i]);
            }
        }

        
        function setVocationString (script) {
            var vocations = '';
            if (script.EK)
                (vocations == ''? vocations += 'Knight' : vocations += ', Knight');
            if (script.ED)
                (vocations == ''? vocations += 'Druid' : vocations += ', Druid');
            if (script.MS)
                (vocations == ''? vocations += 'Sorcerer' : vocations += ', Sorcerer');
            if (script.RP)
                (vocations == ''? vocations += 'Paladin' : vocations += ', Paladin');
            
            return vocations;
        }            
     
        
    }
    
    
})();