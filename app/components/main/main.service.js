(function () {
    'use strict';

    angular
        .module('windScripts')
        .service('MainService', MainService);

    MainService.$inject = ['$http', 'GLOBALS', '$timeout'];
    
    function MainService ($http, GLOBALS, $timeout) {
        
        this.allScripts;
        
        return {
            getScripts: getScripts,
            allScripts : this.allScripts
        };
        
        
        function getScripts() {
            return $http.get(GLOBALS().apiUrl)
                .then(onSuccess)
                .catch(onError);
        }
        
        function onSuccess (response) { 
            return response.data;
        };
        
        function onError (error) {
            return error.data;
        };
    }
    
    
})();