(function() {
    'use strict';

    angular
        .module('windScripts')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['MainService', 'CommonService'];
    
    function HomeController(MainService, CommonService){
        var vm = this;                
        
        if (!MainService.allScripts) {                
            getData()
                .then(function (data) {          
                    vm.scriptsArray = data;                    
                    MainService.allScripts = data;                    
                    setAllVocationStrings(vm.scriptsArray);
                });            
            
        } else {
            vm.scriptsArray = MainService.allScripts;
            setAllVocationStrings(vm.scriptsArray);
        }
        
        function setAllVocationStrings (allScripts) {
            return CommonService.setAllVocationStrings(allScripts);            
        }

        function getData () {
            return MainService.getScripts()
                .then(function (data) {
                    return data;
                });
        }   
         
    }
    
})();