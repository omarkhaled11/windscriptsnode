(function(){
    'use strict';

    angular
        .module('windScripts')
        .directive('navBar', navBar);

    navBar.$inject = ['$rootScope', '$timeout'];

    function  navBar($rootScope, $timeout) {
        var directive = {
            restrict: 'EA',
            templateUrl: 'app/components/navbar/navbar.view.html',
            controller: NavbarController,            
            bindToController: true
        };

        return directive;        
    }
    
    NavbarController.$inject = ['$rootScope', 'AuthService', '$scope', 'store', 'jwtHelper'];
    
    function NavbarController($rootScope, AuthService, $scope, store, jwtHelper){   
        $scope.userLink = '';     
        $scope.adminUser = false;
        
        // set navbar items on page refresh
        if (store.get('jwt')) {
            $scope.authenticated = true;    
            var jwt = store.get('jwt');
            var decodedJwt = jwt && jwtHelper.decodeToken(jwt);
            $scope.userLink = decodedJwt.user;
            if (decodedJwt.admin)
                $scope.adminUser = true;
            
        } else {
            $scope.authenticated = false;
            $scope.userLink = '';
        }
            
        // set navbar items on authentication change
        $scope.$on('isAuthenticated', function (e, data) {
            $scope.authenticated = data.authenticated; 
            if ($scope.authenticated) {
                var jwt = store.get('jwt');
                var decodedJwt = jwt && jwtHelper.decodeToken(jwt);                
                $scope.userLink = decodedJwt.user;
                if (decodedJwt.admin)
                    $scope.adminUser = true;
            }            
        })
    }
    
})();