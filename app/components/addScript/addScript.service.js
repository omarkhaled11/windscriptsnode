(function () {
    'use strict';

    angular
        .module('windScripts')
        .service('AddScriptService', AddScriptService);

    AddScriptService.$inject = ['$http', 'GLOBALS'];
    
    function AddScriptService ($http, GLOBALS) {
        return {
            addScript: addScript,
            uploadFile: uploadFile
        };

        function addScript(script) {
            return $http.post(GLOBALS().apiUrl, script)
                .then(onSuccess)
                .catch(onError);
        }
        
        function uploadFile () {                       
            var totalFiles = document.getElementById("FileUpload").files.length;                
            var formData = new FormData();
            var scriptFile;            
            
            for(var i = 0; i < totalFiles; i++)
            {
                scriptFile = document.getElementById("FileUpload").files[0];
                formData.append("FileUpload", scriptFile);
                
                return $http.post(GLOBALS().uploadUrl, formData, {  
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                .then(onSuccess)
                .catch(onError);
            }
        }
        
        function onSuccess (response) {
            return response.data;
        };
        
        function onError (error) {
            console.log(error);
            return error.data;
        };
    }
    
    
})();