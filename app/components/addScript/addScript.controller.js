(function () {
    'use strict';

    angular
        .module('windScripts')
        .controller('AddScriptController', AddScriptController);

    AddScriptController.$inject = ['AddScriptService', '$state', 'GLOBALS','MainService', 'store', 'jwtHelper', '$scope'];
    
    function AddScriptController(AddScriptService, $state, GLOBALS, MainService, store, jwtHelper, $scope){
        var vm = this;
        vm.script = {};
        vm.setScript = setScript;
        vm.uploadFile = uploadFile;
        vm.uploadIcon = false;        
        vm.submitForm = submitForm;
        
        function submitForm (isValid, script) {
            if (isValid)
                console.log(script);
            else
                console.log('invalid');
//                setScript(script);
        }
        
        
        function setScript(script) {
            // Difficulty
            var Difficulty = $("#Difficulty").find("label.active").find("input").prop('value');
            if (typeof Difficulty === 'undefined')
                vm.script.Difficulty = "Medium";
            else
                vm.script.Difficulty = Difficulty;

            // gold and coin checkboxes
            (document.getElementById('GoldChBox').checked ? vm.script.TibiaGold = true : vm.script.TibiaGold = false);
            (document.getElementById('CoinChBox').checked ? vm.script.TibiaCoin = true : vm.script.TibiaCoin = false);        

            // vocations
            vm.script.EK = checkVocation("EK");
            vm.script.ED = checkVocation("ED");
            vm.script.MS = checkVocation("MS");
            vm.script.RP = checkVocation("RP");
            
            // hotkeys      
            var hotKeysArray = [];
            var keys = document.getElementById('KeyList').getElementsByTagName('li');            
            var shiftKeys = document.getElementById('ShiftKeyList').getElementsByTagName('li');            
            
            for (var i = 0; i < keys.length; i++) {                
                var keyName = keys[i].getElementsByTagName('input')[0].id;                
                var keyValue = document.getElementById(keyName).value;
                var hotKey = {
                    keyName: keyName,
                    keyValue: keyValue
                };
                if (keyValue && keyValue != ' ')
                    hotKeysArray.push(hotKey);            
            }
            for (var i = 0; i < shiftKeys.length; i++) {                
                var keyName = shiftKeys[i].getElementsByTagName('input')[0].id;                
                var keyValue = document.getElementById(keyName).value;
                var hotKey = {
                    keyName: keyName,
                    keyValue: keyValue
                };
                if (keyValue && keyValue != ' ')
                    hotKeysArray.push(hotKey);            
            }            
            vm.script.HotKeys = hotKeysArray;

            
            // set Author
            if (store.get('jwt')) {
                var jwt = store.get('jwt');
                var decodedJwt = jwt && jwtHelper.decodeToken(jwt);
                vm.script.Author = decodedJwt.user;                
            }
            
            
            // set purchase count
            vm.script.PurchaseCount = 0;
            console.log(vm.script);
            
            
            addScript(script)
                .then(function (data){                
                    MainService.allScripts = "";
                    console.log(data);
                    var jwt = store.get('jwt');
                    var decodedJwt = jwt && jwtHelper.decodeToken(jwt);
                    $state.go('user', {username: decodedJwt.user});                
                });
        }
        
        function addScript(script) {
            return AddScriptService.addScript(script)
                .then(function (data){
                    return data; 
                });
        }

        function uploadFile () {
            vm.uploadIcon = true;
            AddScriptService.uploadFile()
                .then(function (data){
                    vm.uploadIcon = false;
                    vm.result = data.message;
                    vm.script.FilePath = data.name;
                });
        }                        
                                      
        function checkVocation(vocation) {
            var voc = $("#Vocation").find("label.active").find("input." + vocation).prop('value');            
            return voc? true : false;
        }
    }  
      
    
})();

// enable upload button after checking file input, had be outside IIFE 
// since it's made in raw JS

function onUploadChange () {    
    document.getElementById("uploadBTN").removeAttribute('disabled');                
}   