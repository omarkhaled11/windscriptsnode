(function() {
    'use strict';

    angular
        .module('windScripts')
        .controller('VocationController', VocationController);

    VocationController.$inject = ['$state', 'MainService', 'CommonService'];
    
    function VocationController ($state, MainService, CommonService){
        var vm = this;
        vm.vocation = $state.params.vocation;
        vm.scriptsByVocation = [];
        
        if (!MainService.allScripts) {
            getData()
                .then(function (data) {   
                    filterVocation(data);
                    MainService.allScripts = data;
                    setAllVocationStrings(vm.scriptsByVocation);
                });
            
        } else {
            filterVocation(MainService.allScripts);
            setAllVocationStrings(vm.scriptsByVocation);
        }            
        
        
        function getData () {
            return MainService.getScripts()                
                .then(function (data) {                    
                    return data;
                });
        }      
        
        function setAllVocationStrings (allScripts) {
            return CommonService.setAllVocationStrings(allScripts);            
        }
        
        function filterVocation (allScripts) {
            for(var i = 0; i < allScripts.length; i++) {            
                switch (vm.vocation) {
                    case 'knight':
                        if (allScripts[i].EK)
                            vm.scriptsByVocation.push(allScripts[i]);
                        break;
                    case 'druid':
                        if (allScripts[i].ED)
                            vm.scriptsByVocation.push(allScripts[i]);
                        break;
                    case 'sorcerer':
                        if (allScripts[i].MS)
                            vm.scriptsByVocation.push(allScripts[i]);
                        break;
                    case 'paladin':
                        if (allScripts[i].RP)
                            vm.scriptsByVocation.push(allScripts[i]);
                        break;
                    default:
                        $state.go('home');
                }
            }                        
//            console.log(vm.scriptsByVocation); 
        }
        
        String.prototype.capitalizeFirstLetter = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }
        
        
    }
})();


//    var timestamp = vm.scriptsByVocation[0]._id
//    timestamp = timestamp.toString().substring(0,8);
//    timestamp = new Date( parseInt( timestamp, 16 ) * 1000 );
//    console.log(timestamp);