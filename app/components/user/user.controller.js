(function() {
    'use strict';

    angular
        .module('windScripts')
        .controller('UserController', UserController);

    UserController.$inject = ['$state', 'MainService'];
    
    function UserController ($state, MainService){
        var vm = this;
        vm.username = $state.params.username;        
        vm.scriptsByAuthor = [];
        
        if (!MainService.allScripts) {
            getData()
                .then(function (data) {   
                    filterAuthor(data);
                    MainService.allScripts = data;                    
                });
            
        } else {
            filterAuthor(MainService.allScripts);            
        }            
        
        
        function getData () {
            return MainService.getScripts()                
                .then(function (data) {                    
                    return data;
                });
        }
        
        function filterAuthor (allScripts) {
            for (var i = 0; i< allScripts.length; i++) {
                if (allScripts[i].Author == vm.username)
                    vm.scriptsByAuthor.push(allScripts[i]);
            }
        }
    }
})();