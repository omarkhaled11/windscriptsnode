var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var tibiaScriptSchema = new Schema ({
    Title: String,
    Requirements: String,
    Features: String,
    Difficulty: String,
    FilePath: String,
    LevelFrom: Number,
    LevelTo: Number,
    ExpFrom: Number,
    ExpTo: Number,
    LootFrom: Number,
    LootTo: Number,
    Location: String,
    Comment: String,
    Free: Boolean,
    PurchaseCount: Number,
    Author: String,
    VideoURL: String,
    HeaderImgUrl: String,    
    LocationImgUrl: String,    
    BpImgUrl1: String,
    BpImgUrl2: String,
    EK: Boolean,
    ED: Boolean,
    MS: Boolean,
    RP: Boolean,
    TibiaGold: Boolean,
    TibiaGoldRetro: Number,
    TibiaGoldOther: Number,
    TibiaCoin: Boolean,
    TibiaCoinRetro: Number,
    TibiaCoinOther: Number,
    Paypal: Number,
    HotKeys: [Schema.Types.Mixed]
});

module.exports = mongoose.model('tibiaScript', tibiaScriptSchema);
