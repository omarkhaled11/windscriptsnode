(function () {
// configure routes 
   angular
    .module('windScripts')    
    .config(config)
    .run(run)
    .filter('trustAsResourceUrl', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);  // This is for the iframe to display Video
    };
}])
   
function config($stateProvider, $urlRouterProvider, $locationProvider){    

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'app/components/home/home.view.html',
            controller: 'HomeController as vm',
            data: { pageTitle: 'Home' }
        })
        .state('vocation', {
            url: '/vocation/:vocation',
            templateUrl: 'app/components/vocation/vocation.view.html',
            controller: 'VocationController as vm',
            data: { pageTitle: 'Vocation' }
        })
        .state('tibiascript', {
            url: '/tibiascript/:script_Id',
            templateUrl: 'app/components/tibiaScript/tibiaScript.view.html',
            controller: 'TibiaScriptController as vm'  ,
            data: {
                pageTitle: 'Script'
            }
        })   
        .state('addscript', {
            url: '/addscript',
            templateUrl: 'app/components/addScript/addScript.view.html',
            controller: 'AddScriptController as vm',
            data: {
                pageTitle: 'Add Script',
                requiresLogin: true
            }
        })   
        .state('user', {
            url: '/user/:username',
            templateUrl: 'app/components/user/user.view.html',
            controller: 'UserController as vm',
            data: {
                pageTitle: 'Profile',
            }
        })   
        .state('login', {
            url: '/login?ReturnUrl',
            templateUrl: 'app/components/authentication/login.view.html',
            controller: 'LoginController as vm',
            data: {
                pageTitle: 'Login',
                login: true
            }
        })
        .state('signup', {
            url: '/signup',
            templateUrl: 'app/components/authentication/signup.view.html',
            controller: 'SignupController as vm',
            data: {
                pageTitle: 'Signup',
                signup: true
            }
        })
        .state('logout', {
            url: '/logout',            
            controller: 'NavbarController as vm',
            data: {
                pageTitle: 'Logout',
                logout: true
            }
        })
        .state('windadmin', {
            url: '/windadmin',
            templateUrl: 'app/components/admin/admin.view.html',
            controller: 'AdminController as vm',
            data: {
                pageTitle: 'Admin',
                requiresLogin: true,
                admin: true
            }
        })   
        .state('about', {
            url: '/about',
            templateUrl: 'app/components/about/about.view.html',
            controller: 'AboutController as vm',
            data: {
                pageTitle: 'About Us'
            }
        })   
    
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
}    
    
function run ($state, store, $rootScope, jwtHelper) {
    
    $rootScope.$on('$stateChangeStart', function(e, to){
        if (to.data && to.data.requiresLogin){
            // redirect to login with returnUrl if not logged in
            if (!store.get('jwt')) {
                e.preventDefault();
                $state.go('login', {ReturnUrl: to.name});
            }
        }
        // logout user and remove token
        if (to.data && to.data.logout){            
            e.preventDefault();
            if (store.get('jwt')) 
                store.remove('jwt');
                
            $rootScope.$broadcast('isAuthenticated', {authenticated: false}); 
            
            if ($state.current.name == 'home')
                $state.go($state.current, {}, {reload: true});
            else    
                $state.go('home');   
            
        }
        
        // on state login/signup while logged in, redirect to home
        if (to.data && (to.data.login || to.data.signup)){            
            if (store.get('jwt')) {
                e.preventDefault();
                $state.go('home');
            }      
        }
        
        // check admin status on admin pages
        if (to.data && to.data.admin) {
            var jwt = store.get('jwt');
            var decodedJwt = jwt && jwtHelper.decodeToken(jwt);
            console.log(decodedJwt);
            if (!decodedJwt.admin)
                e.preventDefault();
                $state.go('home');
        }
        
        
        // check token on every state change
        if (store.get('jwt')) 
            $rootScope.$broadcast('isAuthenticated', {authenticated: true}); 
        else
            $rootScope.$broadcast('isAuthenticated', {authenticated: false}); 
    });
        
} 
    
}());