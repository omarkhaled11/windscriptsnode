(function () {
// configure animation
   angular
    .module('windScripts')    
    .animation('.reveal-animation', animation);

function animation () {
    return {
        enter: function(element, done) {
            element.css('display', 'none');
            element.fadeIn(400, done);
            return function() {
                element.stop(); 
            }
        },
        leave: function(element, done) {
            element.fadeOut(0, done)
            return function() {
                element.stop();
            }
        }
    }
} 
}())