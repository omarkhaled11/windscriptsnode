(function () {
    
    angular
        .module('windScripts')
        .constant('GLOBALS', constant)
    
    // get values by injecting 'GLOBALS' and calling 'GLOBALS().property'.
    
    // this file is in .gitignore to modify remove it first.
    
    
    function constant () {
//        this.host = 'http://windscripts.cloudapp.net';
        this.host = 'http://localhost';        
        this.apiPort = '8090'
        
        return {
            apiUrl: this.host + ':' + this.apiPort + '/api/scripts/',
            uploadUrl: this.host + ':' + this.apiPort + '/api/scripts/uploadscript/',
            downloadUrl: this.host + ':' + this.apiPort + '/api/scripts/downloadscript/',
            usersApi: this.host + ':' + this.apiPort + '/api/'
        };        
    };
    
}());