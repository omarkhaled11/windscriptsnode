(function () {
    'use strict';

    angular
        .module('windScripts')
        .directive('youtubeHelp', youtubeHelp);

    function  youtubeHelp() {
        var directive = {
            restrict: 'AE',
            scope: {
                header: '@',
              videolink: '@'
            },
            transclude: true,
            replace: true,
            template: '<iframe width="100%" height="315" ng-src="{{videolink | trustAsResourceUrl}}" frameborder="0" allowfullscreen></iframe>',
            link: function(scope, element, attrs) {                      
//                scope.header = attrs.header;                
            }
        };
        return directive;
    }

})();