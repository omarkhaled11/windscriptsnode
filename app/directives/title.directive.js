(function(){
    'use strict';

    angular
        .module('windScripts')
        .directive('updateTitle', updateTitle);

    updateTitle.$inject = ['$rootScope', '$timeout'];

    function  updateTitle($rootScope, $timeout) {
        var directive = {
            link: link,
            restrict: 'EA',
        };

        return directive;
        
        function link (scope, element) {

            var listener = function(event, toState) {

                var title = 'Wind Scripts';
                if (toState.data && toState.data.pageTitle) 
                    title = 'Wind Scripts - ' + toState.data.pageTitle;

                $timeout(function() {
                    element.text(title);
                }, 0, false);
            };

            $rootScope.$on('$stateChangeSuccess', listener);
        }
        
    }
    
})();