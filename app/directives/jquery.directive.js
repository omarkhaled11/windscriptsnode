(function () { 
    
'use strict';

angular
    .module('windScripts')
    .directive('jqueryDir', jqueryDir);            

function  jqueryDir() {
    var directive = {
        link: link,
        restrict: 'EA',
    };
    return directive;

    function link(scope, element, attrs, controller) {

        //Jquery code
        
        $('#ShiftBTN').click(function (){
            console.log('clck');
            if ($('#ShiftBTN').hasClass('active')) {
                $('#ShiftKeyList').hide();
                $('#KeyList').show();
            } else {
                $('#KeyList').hide();
                $('#ShiftKeyList').show();
            }
        });

        $("#GoldChBox").change(function () {
            $(".TibiaGold").toggle(200);
        });

        $("#CoinChBox").change(function () {
            $(".TibiaCoin").toggle(200);
        });

        $("#FreeChBox").change(function () {
            if (this.checked)
                $("#PaymentInputs").hide(200);
        });

        $("#PaidChBox").change(function () {
            if (this.checked)
                $("#PaymentInputs").show(200);    
        });
        
        // navbar

        $(".navItem").click(function () {
            $('.navbar-collapse').collapse('hide');
        });
        
        
        //Jquery end
    }
    
}
               
} ());