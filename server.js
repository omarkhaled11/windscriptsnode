var config = require('./config');
var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var busboy = require('connect-busboy');
var tibiaScriptRoutes = require('./routes/tibiaScriptRoutes');
var User = require('./routes/userRoutes');

console.log("Starting");

// EXPRESS
var app = express();

app.use(express.static(__dirname));
app.all('/*', function(req, res, next) {
    res.sendFile('index.html', { root: __dirname });
});

app.listen(config.sitePort, function () {
    console.log("Listening on: " + config.host + ":" + config.sitePort);
});


// REST api 
var api = express();

api.use(cors());
api.use(busboy());
api.use(bodyParser.urlencoded({extended: true}));
api.use(bodyParser.json());
api.set('superSecret', config.secret);

tibiaScriptRoutes.setTibiaScriptRoutes(api);
User.setUserRoutes(api, config);

api.listen(config.apiPort, function () {
    console.log("Listening on: " + config.host + ":" + config.apiPort + " - API");
});


// MongoDB connection
var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect(config.database);
