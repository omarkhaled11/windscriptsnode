module.exports = {
    'host': 'localhost',
    'sitePort': '3000',
    'apiPort': '8090',
    'secret': 'secret123',
    'database': 'mongodb://localhost/windscriptsDB'
};