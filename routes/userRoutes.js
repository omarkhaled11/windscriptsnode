module.exports = {
    setUserRoutes: setUserRoutes
};

var User = require('../app/models/user.model');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs');

function setUserRoutes (api, config) {
    checkAdmin();               // Check/Add admin account
    jwtMiddleWare(api, config); // Authentication Middleware.
    getAllUsers(api);           // /api/users           GET
    getUser(api);               // /api/users/:userid   GET
    updateUser(api);            // /api/users/:userid   PUT
    deleteUser(api);            // /api/users/:userid   DELETE
    login(api, config);         // /api/login           POST
    signup(api, config);        // /api/signup          POST
}

function jwtMiddleWare (api, config) {
    api.use('/api/users', function (req, res, next){        
        
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        
        if (token){
            var decoded = jwt.decode(token);
            if (decoded.admin){
                jwt.verify(token, config.secret, function (err, decoded){
                    if (err){                    
                        return res.status(401).send({returnResult: false, message: 'Failed to authenticate token.'});         
                    } else {
                        next();
                    }
                });
            } else {
                return res.status(401).send({returnResult: false, message: 'permission denied.'});  
            }
        } else {
            return res.status(401).send({returnResult: false, message: 'token not found'});
        }        
    });
}

function checkAdmin () {    
    User.findOne({
        username: 'windscriptsadmin'
    }, function (err, user) {
        if (!user){
            var user = new User ({
                username: 'windscriptsadmin',
                password: 'harhoosh123',
                admin: true
            });  
        
            user.save(function(err){
               if(err){                   
                   console.log('Admin not saved - ', Date());
                   res.send(err);
               }
                                   
                console.log('Admin account created.');
            });
        } else {
            console.log('Admin account exists');
        }
    });
                
}

function getUser (api) {
    api.get('/api/users/:user_id', function(req, res){
    
        User.findById(req.params.user_id, function(err, user){
           if(err){
                console.log("GET: /api/users/:user_id  - ", Date());
                res.send(err);
           }
            
            res.json(user);                    
        });
    });
}

function getAllUsers (api) {
    api.get('/api/users', function(req, res){
        User.find(function(err, users){
            if(err){            
                console.log('GET: /api/users - ', Date());
                res.send(err);
            }

            res.json(users);
        });           
    });     
}

function updateUser (api) {
    api.put('/api/users/:user_id', function( req, res){
    
        User.findById(req.params.user_id, function(err, user){
           if(err){
               console.log('PUT: /api/users/:user_id - ', Date());
               res.send(err);
           }
                           
            var newUser = new User(user);

            newUser.save(function(err) {
                if (err){
                    console.log('PUT: /api/users/:user_id - ', Date());
                    res.send(err);
                }
                    
                res.json({ message: 'User updated! Id: ' + newUser.username });
            });
        });
    });
}

function deleteUser (api) {
    api.delete('/api/users/:user_id', function (req, res){
       User.remove({
           _id: req.params.user_id
       }, function(err, user){
            if (err){
                console.log('DELETE: /api/users/:user_id', Date());
                res.send(err);
            }
                
           res.json({message: 'User deleted!'});
       });        
    });
}

function login (api, config) {
    api.post('/api/login', function (req, res){       
        
        var passCompareResult;
        
        if (!req.body.username || !req.body.password)
            return res.status(400).send({returnResult: false, message: 'You must provide both Username and Password to Login.'});
        
        User.findOne({
            username: req.body.username
        }, function(err, user){
                if (err){
                    console.log('POST: /api/login - ', Date());
                    throw err;                                
                }                    
            
                if(!user) 
                    return res.json({returnResult: false, message: 'Username is incorrect, or  not found.'});
                
                var DbHash = user.password;
                bcrypt.compare(req.body.password, DbHash, function(err, result) {
                    if (!result) {                      
                        return res.json({returnResult: false, message: 'Wrong Password.'});
                    } else {
                        var tokenData = {user: user.username, userId: user._id, admin: user.admin};
                        var token = jwt.sign(tokenData, config.secret,{
                            expiresIn: 60*60*24 // 24 hours
                        });
                    return res.json({
                        returnResult: true,
                        message: 'Login Successful',
                        token: token
                    });

                    }
                })            
        });            
    });
}

function signup (api, config) {
    api.post('/api/signup', function(req, res){
        
        if (!req.body.username || !req.body.password)
            return res.status(400).send({returnResult: false, message: 'You must provide both Username and Password to signup.'});
        
        User.findOne({
            username: req.body.username
        }, function (err, user) {
            if (!user){
                var user = new User ({
                    username: req.body.username,
                    password: req.body.password,
                    admin: false
                });    

                user.save(function(err){
                   if(err){               
                       console.log('POST: /api/signup - ', Date());
                       res.send(err);
                   }

                    var tokenData = {user: user.username, admin: false};
                    var token = jwt.sign(tokenData, config.secret,{
                        expiresIn: 60*60*24 // 24 hours
                    });
                    return res.json({returnResult: true, token: token, message: 'Account Created Successfully!'})
                });
                console.log('New user created.');  

            } else {
                return res.status(400).send({returnResult: false, message: 'Username already exists.'});
            }
                
        });                  
    });
}