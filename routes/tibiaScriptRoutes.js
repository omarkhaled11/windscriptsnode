module.exports = {
    setTibiaScriptRoutes: setTibiaScriptRoutes
};

var tibiaScript = require('../app/models/tibiaScript.model');
var path = require('path');
var fs = require('fs-extra'); 

function setTibiaScriptRoutes (api) {
    getAllScripts (api);      // /api/scripts                           GET
    getSingleScript (api);    // /api/scripts/:script_ID                GET
    addScript (api);          // /api/scripts                           POST
    updateScript (api);       // /api/scripts/:script_ID                PUT
    deleteScript (api);       // /api/scripts/:script_ID                DELETE
    uploadScript (api);       // /api/scripts/uploadscript              POST
    downloadScript (api);     ///api/scripts/downloadscript/:script_id  GET
}


function getAllScripts (api) {
    api.get('/api/scripts', function(req, res){

        tibiaScript.find(function(err, scripts){
            if(err){
                console.log("GET: /api/scripts ", Date());
                res.send(err);
            }
                
            res.json(scripts);
        });        
    });
}

function getSingleScript (api) {
    api.get('/api/scripts/:script_id', function(req, res){
    
        tibiaScript.findById(req.params.script_id, function(err, scriptModel){
           if(err){
                console.log("GET: /api/scripts/:script_id ", Date());
                res.send(err);
           }
               
            res.json(scriptModel);                    
        });        
    });
}


function addScript (api) {
    api.post('/api/scripts', function(req, res){

        var scriptModel = new tibiaScript(req.body);        

        scriptModel.save(function(err){
            if(err){
                console.log("POST: /api/scripts ", Date());  
                res.send(err);
            }
                
            res.json({message: 'Script Added! Name:' + scriptModel.Title});
        });            
    });
}

function updateScript (api) {
    api.put('/api/scripts/:script_id', function( req, res){
    
        tibiaScript.findById(request.params.script_id, function(err, script){
           if(err)
               res.send(err);

            script.name = req.body.name;
            script.location = req.body.location;
            script.headerImgUrl = req.body.headerImgUrl;
            script.vocation = req.body.vocation;

            script.save(function(err) {
                if (err){
                    console.log("POST: /api/scripts ", Date());
                    res.send(err);
                }
                    
                res.json({ message: 'Script updated! Id: ' + req.params.script_id });
            });
        });           
    });
}


function deleteScript (api) {
    api.delete('/api/scripts/:script_id', function (req, res){
       tibiaScript.remove({
           _id: req.params.script_id
       }, function(err, script){
            if (err){
                console.log("DELETE: /api/scripts ", Date());
                res.send(err);
            }                

           res.json({message: 'Script deleted! Id: ' + req.params.script_id });
       });        
    });
}

function uploadScript (api) {
    api.post('/api/scripts/uploadscript', function (req, res) {
        console.log('got upload req');
        var fstream;
        req.pipe(req.busboy);
        req.busboy.on('file', function (fieldname, file, filename) {
            console.log("Uploading: " + filename);
            
            //Path where file will be uploaded            
            filename = Date.now();
            fstream = fs.createWriteStream('./uploads/' + filename);
            file.pipe(fstream);
            fstream.on('close', function () {    
                console.log("Upload Finished of " + filename);              
                res.json({message: 'Upload succeeded', name: filename});
            });
        });        
    });
}

function downloadScript (api) {
    api.get('/api/scripts/downloadscript/:script_id', function (req, res) {
        var fstream;
        tibiaScript.findById(req.params.script_id, function(err, scriptModel){
           if(err){
               console.log('/api/scripts/downloadscript/', Date());
               res.send(err);
           }
               
            var file = './uploads/' + scriptModel.FilePath;
            res.download(file, scriptModel.Title);
                              
        });
    });
}
